#version 150

in vec4 color;
in vec3 normal;
in vec4 vert;

in vec2 texCoords;
in float depth;
in vec4 shadowmap_Position;

uniform sampler2D shadowMap;
uniform mat4 lightViewMatrix;


out vec4 out_Color;

void main(void)
{
	//PHONG
    vec3 lightDirection = (lightViewMatrix * vec4(0.0, 0.0, 1.0, 0.0)).xyz;
    lightDirection = normalize(lightDirection);

    vec3 lightReflection = reflect(-lightDirection, normal);
    vec3 eyeDirection = -normalize(vert.xyz);
    float shininess = 100.0;

    vec4 ambientLight = color * 0.2;
    vec4 diffuseLight = color * 0.4 * max(dot(lightDirection, normal),0.0);
    vec4 specularLight = vec4(1.0) *  max(pow(dot(lightReflection, eyeDirection), shininess),0.0);

    //SHADOWMAP
    float shadow = 1.0;
    float spec = 1.0;
    
    vec2 sMPos = shadowmap_Position.xy;
    vec4 projectToLight = texture(shadowMap, sMPos);
    if (projectToLight.z + 0.05 < shadowmap_Position.z) 
    {
    	shadow = 0.0;
    	spec = 0.0;
    }

    //COMPOSE
    out_Color = vec4(shadow*(ambientLight.xyz + diffuseLight.xyz + spec*specularLight.xyz),1.0);
    //out_Color = vec4(projectToLight.z,projectToLight.z,projectToLight.z,1.0);

}
