#version 150

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoords;
in vec4 in_Color;

out vec4 color;
out vec3 normal;
out vec4 vert;
out vec2 texCoords;
out vec4 shadowmap_Position;

out float depth;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 lightViewMatrix;
//uniform mat4 lightProjectionMatrix;

uniform vec4 default_color;

// if attribute in_Color exists or not
uniform bool has_color;

void main(void)
{
	mat4 modelViewMatrix = viewMatrix * modelMatrix;

	mat4 normalMatrix = transpose(inverse(modelViewMatrix));
    normal = normalize(vec3(normalMatrix * vec4(in_Normal,0.0)).xyz);

	vert = modelViewMatrix * in_Position;

	depth = in_Position.z;

	texCoords = in_TexCoords;
    
    if (has_color)
        color = default_color;//in_Color;
    else
        color = default_color;  

	shadowmap_Position = (projectionMatrix * lightViewMatrix * modelMatrix) * in_Position;
	shadowmap_Position.xy = (vec2(1)+(shadowmap_Position.xy/shadowmap_Position.w))/2;

	gl_Position = (projectionMatrix * modelViewMatrix) * in_Position;


}
